let express = require('express');
let sqlCom = require('../scripts/sql-comm');
let async = require('async')
let ServerResponse = require('fwsp-server-response');

let serverResponse = new ServerResponse();
serverResponse.enableCORS(true);
express.response.sendError = function (err) {
    serverResponse.sendServerError(this, {
        result: {
            error: err
        }
    });
};
express.response.sendOk = function (result) {
    serverResponse.sendOk(this, {
        result
    });
};

module.exports = function (app) {


    app.post('/login', (req, res) => {
        if (Object.keys(req.body).length <= 0) return res.sendError({
            err: "Please provide valid details"
        })

        var content = req.body
        console.log("Login Details", content);
        var sqlquery ="select * FROM user_info WHERE email = '" + content.email + "' AND password = '" + content.password + "'"
        console.log(sqlquery)
        sqlCom.fireToDB(sqlquery)
            .then((result) => {
                console.log(">>> Login Details", result)
                if (!result || (result.length <= 0)) return res.sendError({
                    err: "Username Or Password is not valid"
                })

                let abc = "abcdefghijklmnopqrstuvwxyz1234567890".split("");
                var token = "";
                for (var i = 0; i < 32; i++) {
                    token += abc[Math.floor(Math.random() * abc.length)];
                }

                console.log("token is ", token)
                res.send({
                   Status:1,
                    Message: " Login Sucessfull",
                    data:result,
                    token: token
                })

            }).catch((err) => {
                console.log(":::::::::::", err);
                res.sendError({
                    err: "User Name or password is invalid"
                })
            })
    })

    app.post('/add-vendor', (req, res) => {
     
      var content = req.body
      console.log("Vendor Details", content);
      var Sqlquery="Insert into vendordetail(vendorcode, vendorname,vendoraddress,gst) values('" + content.vendorcode + "' , '" + content.vendorname + "' , '" + content.vendoraddress + "','" + content.gst + "')";
    console.log("sql query",Sqlquery);
      sqlCom.fireToDB(Sqlquery)
          .then((result) => {
            if(result.length<=0 || !result)
            {
             res.sendOk({
               Status:0,
               Message: "Error Unable to ADD",
               data:null
              })
            }
            else
            {
             res.sendOk({
               Status:1,
               Message: "Vendor Details Added",
               data:result
              })
            }
    })
  })
  app.get('/get-vendor', (req, res) => {

    sqlCom.fireToDB("select * FROM vendordetail")
         .then((result) => {
             console.log("Get Vendor Detail", result)
            
             if(result.length<=0 || !result)
             {
              res.sendOk({
                Status:0,
                Message: "No Details Retrived",
                data:null
               })
             }
             else
             {
              res.sendOk({
                Status:1,
                Message: "Details Retrived",
                data:result
               })
             }
            

         }).catch((err) => {
             console.log(":::::::::::", err);
             res.sendError({
                 err: "Valid Vendor detail"
             })
         })
})
 app.post('/deletevendor',(req, res) => {
  if (Object.keys(req.body).length <= 0) return res.sendError({
    err: "Please provide valid details"
})
   var content = req.body;
   console.log(" DelVendor details",content);

  sqlCom.fireToDB("Delete from vendordetail where ID= '"+ content.ID+"'")
  .then((result) =>{
    if(result.length<=0 || !result)
    {
     res.sendOk({
       Status:0,
       Message: "Error Unable to Delete",
       data:null
      })
    }
    else
    {
     res.sendOk({
       Status:1,
       Message: "Vendor Deleted",
       data:result
      })
    }

  })
})

app.post('/post-delivery-challan', (req, res) => {
  if (Object.keys(req.body).length <= 0) return res.sendError({
      err: "Please provide valid details"
  })




  var content = req.body

//  genchallanFormArr[i] = content;
  console.log("Challan Details", content);
  // for (var i = 0; i < genchallanFormArr.length; i++){
    var values = this.genchallanFormArr;


    var number = new Date().getTime();

    var marterialnumber = 'MD' + number
    var challannumber = number


    for (var i = 0; i < content.genchallanFormArr.length; i++){
  var Sqlquery1="Insert into challan_details(challan_no, material_no, vendorcode,challandate, partnumber, description, quantity, vehiclenumber, remarks,status) values('" + challannumber + "', '" + marterialnumber + "','" + content.vendorcode + "','" + content.challandate + "' ,'" + content.genchallanFormArr[i].partnumber + "' , '" + content.genchallanFormArr[i].description + "','" + content.genchallanFormArr[i].quantity + "', '" + content.genchallanFormArr[i].vehiclenumber + "','" + content.genchallanFormArr[i].remarks + "', 'Pending')";
console.log("sql query",Sqlquery1);

// //for(genchallanFormArr.length)
  sqlCom.fireToDB(Sqlquery1)
      .then((result) => {
        if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to ADD",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Challan Details Added",
           data:result
          })
        }
})
    }
})
app.get('/delivery-challan', (req, res) => {
  //for (var i = 0; i < this.genchallanFormArr.length; i++){
  sqlCom.fireToDB("Select v.vendorcode,v.vendorname, v.vendoraddress,v.gst, g.challan_no, g.material_no,g.challandate, g.status  FROM vendordetail AS v JOIN challan_details AS g ON v.vendorcode = g.vendorcode group by g.challan_no")
       .then((result) => {
           console.log("Get Challan Detail", result)
           if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to Retrive Details",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Challan Details Retrived",
           data:result
          })
        }

       }).catch((err) => {
           console.log(":::::::::::", err);
           res.sendError({
               err: "Challan Vendor detail"
           })
       })
    //  }
})


app.post('/getindividualchallan', (req,res) => {
  var content =req.body;
  sqlCom.fireToDB("Select v.vendorcode,v.vendorname, v.vendoraddress, g.challan_no, g.material_no,g.challandate, g.partnumber, g.description, g.quantity, g.vehiclenumber, g.remarks, g.job_work, g.return_qty, g.scrap_qty, g.return_total_qty, g.return_remark, g.status  FROM vendordetail AS v JOIN challan_details AS g ON v.vendorcode = g.vendorcode where challan_no = '"+ content.challan_no+"' and material_no = '"+ content.material_no +"'")
       .then((result) => {
           console.log("Get Challan Detail", result)
           if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to Retrive Details",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Challan Details Retrived",
           data:result
          })
        }

       }).catch((err) => {
        console.log(":::::::::::", err);
        res.sendError({
            err: "Challan Vendor detail"
        })
    })
})
app.get('/getvendorcode', (req, res) => {

  var sqlquery = "select vendorcode FROM vendordetail";
  console.log("in call")
  sqlCom.fireToDB(sqlquery)
       .then((result) => {
           console.log("Get Vendor code", result)
           if (!result || (result.length <= 0)) return res.sendError({
                err: "Invalid Vendor detail"
           })
           res.sendOk({ 
             msg:'Vendor code Success!',
             info:result
           })

       }).catch((err) => {
           console.log(":::::::::::", err);
           res.sendError({
               err: "Vendor code error"
           })
       })
})
app.post('/post-part-master', (req, res) => {
  



  var content = req.body

//  genchallanFormArr[i] = content;
  console.log("Challan Details", content);
 
  var Sqlquery1="Insert into material_info(partnumber, description,hsn) values('" + content.partnumber + "','" + content.description + "','" + content.hsn + "')";
console.log("sql query",Sqlquery1);

//for(genchallanFormArr.length)
  sqlCom.fireToDB(Sqlquery1)
      .then((result) => {
        if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to ADD",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Part Details Added",
           data:result
          })
        }
        })
   // }
})
app.get('/getpartnumber', (req, res) => {

  var sqlquery = "select partnumber FROM materialinfo";
  console.log("sql query",sqlquery);
  console.log("in call")
  sqlCom.fireToDB(sqlquery)
       .then((result) => {
           console.log("Get Part Master", result)
           if (!result || (result.length <= 0)) return res.sendError({
                err: "Invalid Part Master detail"
           })
           res.sendOk({
             msg:'Part Master Success!',
             info:result
           })

       }).catch((err) => {
           console.log(":::::::::::", err);
           res.sendError({
               err: "Part Master code error"
           })
       })
})
app.get('/getpartmaster', (req, res) => {

  sqlCom.fireToDB("select * FROM material_info")
       .then((result) => {
           console.log("Get Vendor Detail", result)
           if(result.length<=0 || !result)
           {
            res.sendOk({
              Status:0,
              Message: "No Details Retrived",
              data:null
             })
           }
           else
           {
            res.sendOk({
              Status:1,
              Message: "Details Retrived",
              data:result
             })
           }

       }).catch((err) => {
           console.log(":::::::::::", err);
           res.sendError({
               err: "Valid Vendor detail"
           })
       })
})


app.post('/updatevendor', (req,res) => {
  console.log("details retrived to update",req.body)
  var content = req.body;

  var sqlquery = 'update vendordetail set vendorcode = "'+ content.vendorcode+'" , vendorname = "' +content.vendorname+ '",gst = "' +content.gst+ '", vendoraddress = "'+ content.vendoraddress +'" where ID = "'+ content.ID+'" '

  console.log("sql query",sqlquery)

  sqlCom.fireToDB(sqlquery)
      .then((result) => {
        if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to Update",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Details Updated",
          })
        }
        })
})

app.post('/reportFilter', (req,res) => {
  console.log("details retrived to update",req.body)
  var content = req.body;

  var sqlquery = 'select * from material_info where challandate  = "'+ content.sdate+'" , sdate = "' +content.edate+ '"edate = "' 

  console.log("sql query",sqlquery)

  sqlCom.fireToDB(sqlquery)
      .then((result) => {
        if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to Update",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Details Updated",
          })
        }
        })
})

app.post('/updatepartdetails', (req,res) => {
  console.log("details retrived to update",req.body)
  var content = req.body;

  var sqlquery = 'update material_info set partnumber = "'+ content.partnumber+'" , description = "' +content.description+ '", hsn = "' +content.hsn+ '" ,where ID = "'+ content.ID+'" '

  console.log("sql query",sqlquery)

  sqlCom.fireToDB(sqlquery)
      .then((result) => {
        if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to Update",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Details Updated",
          })
        }
        })
})


app.post('/deletepartdetail',(req, res) => {
  if (Object.keys(req.body).length <= 0) return res.sendError({
    err: "Please provide valid details"
})
   var content = req.body;
   console.log(" details",content);
var sqlquery ="Delete from material_info where ID= '"+ content.ID+"'"
console.log("sqlquery",sqlquery)
  sqlCom.fireToDB(sqlquery)
  .then((result) =>{
    if(result.length<=0 || !result)
    {
     res.sendOk({
       Status:0,
       Message: "Error Unable to Delete",
       data:null
      })
    }
    else
    {
     res.sendOk({
       Status:1,
       Message: "Part Details Deleted",
       data:result
      })
    }

  })
})



app.post('/updatereturn', (req,res) => {
  console.log("details retrived to update",req.body)
  var content = req.body;

  var sqlquery = 'update challan_details set job_work = "'+ content.jobwork+'" , return_qty = "' +content.quantity+ '", scrap_qty = "'+ content.scrapqty +'" , return_total_qty = "'+ content.totalqty +'" , return_remark = "'+ content.remark +'" , status = "Completed" where challan_no = "'+ content.challan_no+'" and material_no = "'+content.material_no+'" '

  console.log("sql query",sqlquery)

  sqlCom.fireToDB(sqlquery)
      .then((result) => {
        if(result.length<=0 || !result)
        {
         res.sendOk({
           Status:0,
           Message: "Error Unable to Update",
           data:null
          })
        }
        else
        {
         res.sendOk({
           Status:1,
           Message: "Details Updated",
          })
        }
        })
})
// app.post('/filterinfo',(req,res) => {

// })

app.get('/getchallaninfo', (req, res) => {

  sqlCom.fireToDB("select * FROM challan_details")
       .then((result) => {
           console.log("Get Vendor Detail", result)
           if(result.length<=0 || !result)
           {
            res.sendOk({
              Status:0,
              Message: "No Details Retrived",
              data:null
             })
           }
           else
           {
            res.sendOk({
              Status:1,
              Message: "Details Retrived",
              data:result
             })
           }

       }).catch((err) => {
           console.log(":::::::::::", err);
           res.sendError({
               err: "Valid Vendor detail"
           })
       })
})



}
