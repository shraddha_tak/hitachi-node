var express = require('express');  
var path = require('path');
var app = express();
app.use(express.json({limit: '10mb'}));
app.use(express.urlencoded({extended: false}));
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var sqlCom = require('./scripts/sql-comm');
var ServerResponse = require('fwsp-server-response');
const nodemailer = require('nodemailer');
https = require('https');
const cors = require('cors');
//CROSS Headers
app.use(cors())


let serverResponse = new ServerResponse();
serverResponse.enableCORS(true);
express.response.sendError = function (err) {
    serverResponse.sendServerError(this, {
        result: {
            error: err
        }
    });
};
express.response.sendOk = function (result) {
    serverResponse.sendOk(this, {
        result
    });
};

// Define the port to run on
app.set('port', 5000);

var date = new Date().getTime();
console.log("unique number",date)


app.get('/', (req, res) => {
    res.send('Chassis Brakes DMS')
})

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    req.originalUrl = decodeURIComponent(req.originalUrl);
    next();
});

require('./routes/login-service')(app)


// Listen for requests
var server = app.listen(app.get('port'), function () {
    var port = server.address().port;
    console.log('Magic happens on port ' + port);
});

module.exports = app;
