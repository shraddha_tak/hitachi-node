-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: challan
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `challan_details`
--

DROP TABLE IF EXISTS `challan_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `challan_details` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `challan_no` varchar(30) DEFAULT NULL,
  `material_no` varchar(30) DEFAULT NULL,
  `vendorcode` varchar(20) DEFAULT NULL,
  `challandate` varchar(15) DEFAULT NULL,
  `partnumber` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `quantity` varchar(100) DEFAULT NULL,
   `bean` varchar(20) DEFAULT NULL,
  `vehiclenumber` varchar(20) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `job_work` varchar(50) DEFAULT NULL,
  `return_qty` varchar(100) DEFAULT NULL,
  `scrap_qty` varchar(100) DEFAULT NULL,
  `return_total_qty` varchar(100) DEFAULT NULL,
  `return_remark` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `sdate` varchar(20) DEFAULT NULL,
    `edate` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `challan_details`
--

LOCK TABLES `challan_details` WRITE;  
/*!40000 ALTER TABLE `challan_details` DISABLE KEYS */;
INSERT INTO `challan_details` VALUES (29,'1629274264288','MD1629274264288','900444','2021-08-17','1234567890','C2A ALLOY CASTINGS','50.10','MH-13454545','NO','Test Job work','11000.901','123','56','Test','Completed'),(30,'1629274264288','MD1629274264288','900444','2021-08-17','123453396','1234567','11000.901','MH-13454545','NO Remarks','Test Job work','11000.901','123','56','Test','Completed'),(31,'1629279427426','MD1629279427426','12347878787','2021-08-16','123453396','1234567','1000.90','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(32,'1629345106805','MD1629345106805','1234','2021-08-16','123453396','1234567','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(33,'1629345141054','MD1629345141054','12347878787','2021-08-21','45678','Test part number','50','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(34,'1629345249072','MD1629345249072','900444','2021-08-22','45678','Test part number','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(35,'1629345287995','MD1629345287995','12347878787','2021-08-22','45678','Test part number','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(36,'1629345330195','MD1629345330195','1234','2021-08-25','1234567890','C2A ALLOY CASTINGS','11000.901','MH-13454545','NO Remarks','null','null','null','null','null','Completed'),(37,'1629345461842','MD1629345461842','900444','2021-08-26','45678','Test part number','50','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(38,'1629345545741','MD1629345545741','900444','2021-08-27','45678','Test part number','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(39,'1629345570287','MD1629345570287','900444','2021-08-28','45678','Test part number','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(40,'1629345807444','MD1629345807444','900444','2021-08-29','45678','Test part number','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(41,'1629345835188','MD1629345835188','900444','2021-08-30','1234567890','C2A ALLOY CASTINGS','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(42,'1629345857950','MD1629345857950','900444','2021-08-31','45678','Test part number','11000.901','MH-13454545','NO Remarks',NULL,NULL,NULL,NULL,NULL,'Pending'),(43,'1630477513543','MD1630477513543','1234','2021-09-02','45678','Test part number','11000.901','MH-13454545','NO Remarks','Testing Job Work','50','12','5.5','NO','Completed'),(44,'1630557696526','MD1630557696526','456','2021-09-02','1234567890','C2A ALLOY CASTINGS','50.10','MH-13454545','NO Remarks','Testing Job Work','11000.901','12','5.5','NO','Completed');
/*!40000 ALTER TABLE `challan_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_info`
--

DROP TABLE IF EXISTS `material_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `material_info` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `partnumber` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
   `hsn` varchar(50) DEFAULT NULL,
 
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_info`
--

LOCK TABLES `material_info` WRITE;
/*!40000 ALTER TABLE `material_info` DISABLE KEYS */;
INSERT INTO `material_info` VALUES (8,'45678','Test part number',NULL),(9,'1234567890','C2A ALLOY CASTINGS',NULL),(11,'12345','test',NULL),(12,'987689979','Test_2',NULL);
/*!40000 ALTER TABLE `material_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_info` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (1,'admin@hitachi.com','Hitachi@2021','Admin','Active');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendordetail`
--

DROP TABLE IF EXISTS `vendordetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendordetail` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `vendorcode` varchar(20) DEFAULT NULL,
  `vendorname` varchar(30) DEFAULT NULL,
   `gst` varchar(30) DEFAULT NULL,
  `vendoraddress` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendordetail`
--

LOCK TABLES `vendordetail` WRITE;
/*!40000 ALTER TABLE `vendordetail` DISABLE KEYS */;
INSERT INTO `vendordetail` VALUES (8,'1234','Sourabh Ambarshetti','A-41 Nirmiti Vihar Vijapur Road Solapur',NULL),(22,'456','Test_1','Hadapsar Pune',NULL),(23,'123456','Test_12','Pune 411028',NULL);
/*!40000 ALTER TABLE `vendordetail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-24 13:32:48
