const mysql = require('mysql');
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'Admin@123',
  database: 'challan',
  insecureAuth: true
});


connection.connect(function (err) {
  if (err) throw err;
  console.log("MYSQL DB Connection Established!");
});

async function fireToDB(sqlquery) {
  return await processor(sqlquery)
}


function processor(sqlquery) {
  return new Promise((resolve, reject) => {
    connection.query(sqlquery, function (err, result) {
      if (result)
        var res = Object.values(JSON.parse(JSON.stringify(result)))
      resolve(res);
      if (err)
        reject(err)
    })
  })
}




module.exports = {
  connection,
  fireToDB
}
